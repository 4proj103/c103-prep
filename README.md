# C103-Prep

Helm Chart used to stage a Kubernetes cluster for C103's web application deployment.

## TL;DR

```bash
kubectl create namespace c103
git clone https://gitlab.com/4proj103/c103-helm.git/
cd c103-helm && helm dependency update
helm install -n c103 c103 -f values.yaml .
```

## Introduction

This chart adds the following to the cluster:

* A MongoDB server

* A Mongo-Express web interface

* A reverse proxy allowing authentication with Azure AD to access Mongo-Express

* A Node-Red server

* A service account that can make new deployments

## Prerequisites

* Kubernetes 1.12+

* Helm 3.0+

## Installing

Install the chart using:

```bash
helm install -n c103 c103 .
```

These commands deploy each services on the Kubernetes cluster in the default configuration and with the release name `c103`. The deployment configuration can be customized by specifying the customization parameters with the `helm install` command using the `--values` or `--set` arguments. Find more information in the [configuration section](#configuration) of this document.

## Upgrading

Upgrade the chart deployment using:

```bash
helm upgrade -n c103 c103 .
```

The command upgrades the existing `c103` deployment with the most recent release of the chart.

**TIP**: Use `helm repo update` to update information on available charts in the chart repositories.

## Uninstalling

Uninstall the `c103` deployment using:

```bash
helm uninstall c103
```

The command deletes the release named `c103` and frees all the kubernetes resources associated with the release.

**TIP**: Specify the `--purge` argument to the above command to remove the release from the store and make its name free for later use.

## Configuration

A lot of the values configured in this chart are inherited from their respective subcharts. They can be found at the following locations:

* MongoDB: https://github.com/bitnami/charts/blob/master/bitnami/mongodb/values.yaml
* MongoExpress: https://github.com/cowboysysop/charts/blob/master/charts/mongo-express/values.yaml
* Oauth2-proxy: https://github.com/k8s-at-home/charts/blob/master/charts/oauth2-proxy/values.yaml
* Node-Red: https://github.com/k8s-at-home/charts/blob/master/charts/node-red/values.yaml

The remaining variables that are specific to this very chart are the following:

| Name                                 | Description                                                                                           | Default                                               |
|--------------------------------------|-------------------------------------------------------------------------------------------------------|-------------------------------------------------------|
| `networkpolicy.enabled` | Create or not a network policy that prevents this deployment's pods from communicating with other namespaces | `false` |

Specify the parameters you which to customize using the `--set` argument to the `helm install` command. For instance,

```bash
$ helm install c103 \
    --set networkpolicy.enabled=true c103
```

Alternatively, a YAML file that specifies the values for the above parameters can be provided while installing the chart. For example,

```bash
$ helm install c103 \
    --values values.yaml c103
```

**Tip**: A sample file named [values.yaml](values.yaml) is available in this repository.

## Edge Cases
### Node-Red authentication/plugins in general

The docker image used for Node-Red is the standard one that does not include many different plugins, including those that handle authentication.

Since we're using Azure AD for this project, you will need to install the plugin before implementing the authentication itself, otherwise the container will crash on startup, complaining about the lack of it.

One should instead follow this process:
* Leave Node-Red's adminAuth empty (or implement a basic authentication) 
* Publish the helm release
* Boot up a shell inside Node-Red's contaier, `cd` to `/data`, and run the `npm install` commands to install your auth plugins
* Amend your values file to add your adminAuth back and upgrade the helm release
* Kill Node-Red's current container to apply the new configuration
* Make sure you can login using your custom provider

Here is a sample adminAuth that uses `node-red-contrib-auth-azuread` to log onto Azure AD:
```js
adminAuth:require("node-red-contrib-auth-azuread")({
    identityMetadata: 'https://login.microsoftonline.com/<tenant>/v2.0/.well-known/openid-configuration',
    clientID: '',
    clientSecret: '',
    responseType: 'code',
    responseMode: 'query',
    redirectUrl: 'https://<node>/auth/strategy/callback',
    users: [
        {username: "<user@domain.tld>",permissions: ["*"]}
    ]
})
```
